@extends('layouts.app')

@section('content')
    <header class="header_field">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="logo"></div>
                    <div class="nav-switch">
                        <span>Menu</span>
                        <i class="fa fa-bars"></i>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <a href="#" class="btn header-btn">Вход</a>
                    <nav class="main-menu">
                        <ul>
                            <li><a href="Subjects.html">Предметы</a></li>
                            <li><a href="courses.html">Курсы</a></li>
                            <li><a href="#" onclick="slowScroll('#formname')">Личный кабинет</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="intro">
        <header class="header_field">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="logo">
                            <img src="" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="content_field">
            <div class="container">
                <div class="content_text text-white">
                    <h2>Добро пожаловать на сервис онлайн образования!</h2>
                </div>
            </div>
        </div>
    </div>
    <section id="news" class="blog wow fadeInUp" data-wow-delay="300ms">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
                        Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. Curabitur
                        blandit tempus porttitor. Nullam id dolor id nibh ultricies vehicula ut id elit. Etiam porta sem
                        malesuada magna mollis euismod.</p>
                    <p>Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget
                        metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus,
                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet
                        risus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nulla vitae elit libero, a
                        pharetra augue.</p>
                </div>
                <div class="col-md-5">
                    <a href="#">
                        <img src="https://unsplash.it/1200/1200?image=839" alt="" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="form_container">
        <div class="container">
            <div class="form" id="formname">
                <ul class="tab-group">
                    <li class="tab active"><a href="#signup">Регистрация</a></li>
                    <li class="tab"><a href="#login">Вход</a></li>
                </ul>
                <div class="tab-content">
                    <div id="signup">
                        <h1>Регистрация на портале</h1>
                        <form action="/" method="post">
                            <div class="top-row">
                                <div class="field-wrap">
                                    <label>
                                        Имя<span class="req">*</span>
                                    </label>
                                    <input type="text" required autocomplete="off"/>
                                </div>
                                <div class="field-wrap">
                                    <label>
                                        Фамилия<span class="req">*</span>
                                    </label>
                                    <input type="text" required autocomplete="off"/>
                                </div>
                            </div>
                            <div class="field-wrap">
                                <label>
                                    Почта<span class="req">*</span>
                                </label>
                                <input type="email" required autocomplete="off"/>
                            </div>
                            <div class="field-wrap">
                                <label>
                                    Пароль<span class="req">*</span>
                                </label>
                                <input type="password" required autocomplete="off"/>
                            </div>
                            <button type="submit" class="button button-block">За работу!
                            </button>
                        </form>
                    </div>
                    <div id="login">
                        <form action="/" method="post">
                            <div class="field-wrap">
                                <label>
                                    Почта<span class="req">*</span>
                                </label>
                                <input type="email" required autocomplete="off"/>
                            </div>
                            <div class="field-wrap">
                                <label>
                                    Пароль<span class="req">*</span>
                                </label>
                                <input type="password" required autocomplete="off"/>
                            </div>
                            <p class="forgot"><a href="#">Забыли пароль?</a></p>
                            <button class="button button-block">Войти
                            </button>
                        </form>
                    </div>
                </div><!-- tab-content -->
            </div>
        </div>
    </div>
    <!-- otziv -->
    <div class="container">
        <h2 style="color:floralwhite;padding:10px; ">Отзывы наших преодавателей и учеников</h2>
        <div class="otz">
            <ul class="list-of-our-clients">
                <li style="opacity: 1;">
                    <figure><img src="https://i.ibb.co/hBk9rys/item-1.jpg" alt="Pacient A"></figure>
                    <h5>Джон Доу</h5>
                    <h6>Водитель фуры</h6>
                    <p>Таким образом рамки и место обучения кадров играет важную роль.</p>
                </li>
                <li style="opacity: 1;">
                    <figure><img src="https://i.ibb.co/hBk9rys/item-1.jpg" alt="Pacient B"></figure>
                    <h5>Джон Доу</h5>
                    <h6>Танкист</h6>
                    <p>Таким образом рамки и место обучения кадров играет важную роль.</p>
                </li>
                <li style="opacity: 1;">
                    <figure><img src="https://i.ibb.co/hBk9rys/item-1.jpg" alt="Pacient C"></figure>
                    <h5>Джон Доу</h5>
                    <h6>Студент</h6>
                    <p>Таким образом рамки и место обучения кадров играет важную роль. </p>
                </li>
            </ul>
        </div>
    </div>
@endsection

